provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "terraform" {
ami = "ami-02e60be79e78fef21"
instance_type = "t2.micro"
subnet_id = "subnet-0acf4fd801c3d8881"
iam_instance_profile = "ssm_role"
user_data = <<-EOT
             #!/bin/bash
             sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
             sudo systemctl start amazon-ssm-agent
}
EOT
tags = {
  Name = "terraform"
}
}
terraform {
  backend "s3" {
    bucket = "udaymybucket"
    key    = "terraform.tfstate"
    region = "ap-south-1"
  }
}


